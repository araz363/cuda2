#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <time.h>
#include <helper_cuda.h>
#include <helper_timer.h>
#include "mmio.h"

__global__  
void kernel(const double *x, double *y, const double *val, const int* I, const int* J, const int nz){
    
		int tid=threadIdx.x+blockIdx.x*blockDim.x;
    double sum=0;
  
		if (tid<nz) {	
        y[I[tid]] += val[tid]*x[J[tid]];
		}
    
}

// debuging functions
void init_y(double *a, const int N);
void init_x(double *a, const int N);

int main (int argc, char *argv[]) {

		int ret_code;
    MM_typecode matcode;
    FILE *f;
    int M, N, nz;
		int i, *I, *J;   
    double *val;
 
		if (argc < 2)
		{
			fprintf(stderr, "Usage: %s [martix-market-filename]\n", argv[0]);
			exit(1);
		}
    else    
    { 
        if ((f = fopen(argv[1], "r")) == NULL) 
            exit(1);
    }

    if (mm_read_banner(f, &matcode) != 0)
    {
        printf("Could not process Matrix Market banner.\n");
        exit(1);
    }
		 
		/*  This is how one can screen matrix types if their application */
		/*  only supports a subset of the Matrix Market data types.      */

		if (mm_is_complex(matcode) && mm_is_matrix(matcode) && 
			      mm_is_sparse(matcode) )
		{
			  printf("Sorry, this application does not support ");
			  printf("Market Market type: [%s]\n", mm_typecode_to_str(matcode));
			  exit(1);
		}

		/* find out size of sparse matrix .... */

		if ((ret_code = mm_read_mtx_crd_size(f, &M, &N, &nz)) !=0) {
			  exit(1);
		}
    /* reserve memory for matrices */

    I = (int *) malloc(nz * sizeof(int));
    J = (int *) malloc(nz * sizeof(int));
    val = (double *) malloc(nz * sizeof(double));


    /* NOTE: when reading in doubles, ANSI C requires the use of the "l"  */
    /*   specifier as in "%lg", "%lf", "%le", otherwise errors will occur */
    /*  (ANSI C X3.159-1989, Sec. 4.9.6.2, p. 136 lines 13-15)            */

    for (i=0; i<nz; i++)
    {
        fscanf(f, "%d %d %lg\n", &I[i], &J[i], &val[i]);
        I[i]--;  /* adjust from 1-based to 0-based */
        J[i]--;
    }

    if (f !=stdin) fclose(f);

    /************************/
    /* now write out matrix */
    /************************/

		// y = Ax
    double *x, *y;
    double *_x, *_y, *_val;
		int *_I, *_J;

    x = (double*)malloc(sizeof(double)*N);
    y = (double*)malloc(sizeof(double)*M);
        init_x(x, N);
        init_y(y, M);


		  checkCudaErrors(cudaMalloc((void**)&_x, sizeof(double)*N));
		  checkCudaErrors(cudaMalloc((void**)&_val, sizeof(double)*N*M));
		  checkCudaErrors(cudaMalloc((void**)&_y, sizeof(double)*nz));
			checkCudaErrors(cudaMalloc((void**)&_I, sizeof(int)*nz));
			checkCudaErrors(cudaMalloc((void**)&_J, sizeof(int)*nz));
		
		  checkCudaErrors(cudaMemcpy(_x, x, sizeof(double)*N, cudaMemcpyHostToDevice));
		  checkCudaErrors(cudaMemcpy(_val, val, sizeof(double)*N*M, cudaMemcpyHostToDevice));
			checkCudaErrors(cudaMemcpy(_y, y, sizeof(double)*M, cudaMemcpyHostToDevice));
			checkCudaErrors(cudaMemcpy(_I, I, sizeof(int)*nz, cudaMemcpyHostToDevice));
			checkCudaErrors(cudaMemcpy(_J, J, sizeof(int)*nz, cudaMemcpyHostToDevice));
			
			int BLOCK_DIM = 256; 
			//int GRID_DIM = (nz - 1)/BLOCK_DIM + 1;
			
			//timer
			StopWatchInterface* timer = 0;
			sdkCreateTimer(&timer);
			timer->start();	
    	kernel<<<nz, BLOCK_DIM>>>(_x, _y, _val, _I, _J, nz);
	
			checkCudaErrors(cudaDeviceSynchronize());
			timer->stop();
						
    	checkCudaErrors(cudaMemcpy(y, _y, sizeof(double)*M, cudaMemcpyDeviceToHost));
			
		for (i = 0; i < M; i++)
				printf("\n%d %d %20.19g", I[i], J[i], y[i]);

		printf("\n");
		std::cout << "timer: " << timer->getTime() << std::endl;
		printf("\n");

    cudaFree(_x);
    cudaFree(_y);
		cudaFree(_val);
    cudaFree(_I);
		cudaFree(_J);
	
		free(x);
		free(I);
		free(J);
		free(y);
		free(val);

        return 0;
};

// Initialisation function
void init_y(double *a, const int N) {
				int i;
        for( i=0; i<N; i++)
                a[i] = 0;
}
void init_x(double *a, const int N) {
				int i;
        for(i=0; i<N; i++)
                a[i] = 1;
}

